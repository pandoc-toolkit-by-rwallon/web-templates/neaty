---
# Metadata of the HTML header.
language:
author:
    -
description:
keywords:
    -
date:
header-includes:
    -

# Style.
theme-color:
light-color:
current-nav-color:
text-color:
hljs-theme:
css:
    -

# Document header.
logo:
site-name:
title:
header-image:
    path:
    alt:

# Document footer.
year:
copyright-holder:
---
