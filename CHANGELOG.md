# Changelog

This file describes the evolution of the *Neaty* template for Pandoc.

Note that versions are numbered using the `BREAKING.FEATURE.FIX` scheme.

## Version 0.1.0 (April 2021)

+ HTML metadata can be set from YAML.
+ Colors can be customized from YAML.
+ Syntax highlighting can be customized from YAML.
+ Additionnal CSS files can be specified from YAML.
+ The header of the document can be customized from YAML.
+ The footer of the document can be customized from YAML.
