###############################################################################
#       MAKEFILE FOR EASILY BUILDING A NEATY HTML DOCUMENT WITH PANDOC        #
###############################################################################

##########
# SOURCE #
##########

# The name of the file containing the metadata of the document.
# It may be left blank, e.g., if these data are put in the Markdown source file.
METADATA =

# The name of the Markdown source file to build the document from.
FILENAME =

# The location of Neaty's files.
NEATY_HOME = .

##########
# OUTPUT #
##########

# The name of the output directory, which will contain the final document
# and all the dependencies of Neaty.
OUTDIR = public_html

# The name of the final document.
OUTFILE = index.html

#######################
# CONVERSION SETTINGS #
#######################

# The options to pass to Pandoc when converting from Markdown to HTML.
# See Pandoc's documentation for more details.
PANDOC_OPTS =

# Whether the document must provide a table of contents.
TOC =

# If there is a table of contents, the corresponding option must be added.
ifdef TOC
    PANDOC_OPTS += --toc
endif

###########
# TARGETS #
###########

# Declares non-file targets.
.PHONY: help

# Builds the document from the Markdown source file.
$(OUTDIR)/$(OUTFILE): $(NEATY_HOME)/neaty.pandoc $(OUTDIR) $(METADATA) $(FILENAME)
	pandoc $(PANDOC_OPTS) --template $(NEATY_HOME)/neaty.pandoc $(METADATA) $(FILENAME) --output $(OUTDIR)/$(OUTFILE)

# Creates the output directory, which will contain the final document,
# and puts all the dependencies of Neaty in it.
$(OUTDIR):
	mkdir -p $(OUTDIR)
	cp -r $(NEATY_HOME)/js $(NEATY_HOME)/css $(OUTDIR)

# Prints a message describing the available targets.
help:
	@echo
	@echo "Build your Neaty pages with Pandoc"
	@echo "=================================="
	@echo
	@echo "Available targets are:"
	@echo "    - $(OUTDIR)/$(OUTFILE): builds the document (default)"
	@echo "    - $(OUTDIR): creates the output directory"
	@echo "    - help: displays this help"
	@echo
