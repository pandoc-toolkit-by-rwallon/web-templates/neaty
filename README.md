# Pandoc Template for *Neaty*

[![pipeline status](https://gitlab.com/pandoc-toolkit-by-rwallon/web-templates/neaty/badges/master/pipeline.svg)](https://gitlab.com/pandoc-toolkit-by-rwallon/web-templates/neaty/commits/master)

## Description

This project provides a template allowing an easy customization of the
[*Neaty* HTML Template](https://templatemo.com/tm-501-neaty) for creating HTML
pages with [Pandoc](https://pandoc.org) following *Neaty*'s style.

This template, and in particular the bundled files of *Neaty*, are distributed
under a Creative Commons Attribution 4.0 International License.

[![CC-BY 4.0](https://i.creativecommons.org/l/by/4.0/88x31.png)](https://creativecommons.org/licenses/by/4.0/)

## Requirements

To build your HTML pages using this template, [Pandoc](https://pandoc.org)
at least 2.0 has to be installed on your computer.

*Neaty* files are bundled with this template, so you are not required to
download them.

## Creating your HTML document

This template enables to write amazing *Neaty* HTML pages in plain
[Markdown](https://pandoc.org/MANUAL.html#pandocs-markdown), thanks to the
power of Pandoc.

To create your pages, you will need to put the `css` and `js` directories, the
`Makefile` and the template `neaty.pandoc` inside the directory in
which you have your Markdown source file and the (optional) metadata file.
You may also simply set the path of *Neaty*'s file to the variable `NEATY_HOME`
in the `Makefile`, so that you do not need to move these files each time you
use this template.
It may also be useful if you use this project as a submodule, for instance.

The template we provide supports many customizations, so that you can get the
best of *Neaty* without having to write HTML nor CSS.

You will find below a description of the available customizations.
You may also be interested in our [examples](./examples), which illustrate
various use cases.

## Template settings

To customize the content and appearance of your document, you may set different
variables as YAML metadata.
For convenience, we provide a [`metadata.md`](./metadata.md) file in which all
possible settings are specified, so that you can just set them or remove the
ones which do not fit your needs.

### Metadata of the HTML document

From the YAML configuration, you may specify common metadata that will be put
in the header of the HTML document.

In particular, you may specify the `language` of the document, as well as 
(the list of) its `author`(s) and the `date` on which the document was
published.

To help search engine, you may also specify the `description` of the document
and the `keywords` that correspond to the page.

Finally, with `header-includes`, you may specify a list of raw HTML code that
will be included in the header.

### Customizing styles

This template provides several variables that allow to change the main colors
of your pages.
To this end, you may set any valid CSS color to the following variables:

- `theme-color` for the color of the titles and the menu,
- `light-color` for the color of the entries in the menu,
- `current-nav-color` for the color of the active entries in the menu, and
- `text-color` for the color of the main text.

Additionally, this template uses [`highlight.js`](https://highlightjs.org) for
highlighting syntax inside code blocks.
If you want to change the appearance of such blocks, you may change the theme
by setting the variable `hljs-theme` accordingly.
You may choose any theme among those defined by `highlight.js`, for which
you can find a demo [here](https://highlightjs.org/static/demo/).

You may also specify a list of CSS files to use to customize the style
of your pages, using the `css` list.

### Document header

The main variable to set to customize the header is `title`, which will be used
as title for the page and as level-1 title in the document.
You may also set the `site-name` variable, which is a short title to use in the
menu.
By default, the same value as `title` will be used.

You may also specify a `logo`, that will be used both for the menu and as
favicon.

This template also allows you to set a `header-image`, for which you must
specify the `path` and the `alt` attribute.
This image will be displayed at the top of the page.

### Document footer

You may cutomize the footer of the document by setting the variables `year` and
`copyright-holder` to the appropriate informations about the copyrights of the
page.

## Conversion to HTML

HTML pages based on this template are built using `make`.
To customize the build to your own use case, you may either update the
[`Makefile`](./Makefile) provided in this project, or set the needed variables
while building.

For example, suppose that your Markdown source file is `example.md`, your
metadata file is `metadata.md`, and you want the HTML document to be built
in a directory named `public_html`.
You will then have to type the following command to build your document:

```bash
make METADATA=metadata.md FILENAME=example.md OUTDIR=public_html
```

Note that you may also customize the way Pandoc produces the document by
setting the variable `PANDOC_OPTS` accordingly.
You may also ask for a table of contents by adding `TOC=T` to the command line
above.
For more details, read [Pandoc's User Guide](https://pandoc.org/MANUAL.html).

Type `make help` to have the full list of available targets.
