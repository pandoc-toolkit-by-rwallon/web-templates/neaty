# Use Cases of the *Neaty* Template

This directory contains examples illustrating different use cases of the Pandoc
template for the *Neaty* HTML template.

All the examples are based on the same Markdown source, available
[here](./example.md).
The different outputs are only obtained by using different metadata files.

For each of the examples, you may either read its metadata file (to see how we
configured it) or see the result as a web page.

## Default Configuration

In this example, there are no additional settings.
The document is produced based on the default configuration.

*Live view with table of contents available [here](https://pandoc-toolkit-by-rwallon.gitlab.io/web-templates/neaty/index.html).*

*Live view without table of contents available [here](https://pandoc-toolkit-by-rwallon.gitlab.io/web-templates/neaty/index-notoc.html).*

## Custom Colors

This example does not use the original colors of *Neaty*, but a set of
user-defined colors instead.

*Metadata file available [here](custom-colors.md).*

*Live view with table of contents available [here](https://pandoc-toolkit-by-rwallon.gitlab.io/web-templates/neaty/custom-colors.html).*

*Live view without table of contents available [here](https://pandoc-toolkit-by-rwallon.gitlab.io/web-templates/neaty/custom-colors-notoc.html).*
