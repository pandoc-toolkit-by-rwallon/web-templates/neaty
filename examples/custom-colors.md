---
# Style.
theme-color: "#E7E7E7"
light-color: "#666666"
current-nav-color: "#393B7E"
text-color: "#666666"
hljs-theme: rainbow
---
