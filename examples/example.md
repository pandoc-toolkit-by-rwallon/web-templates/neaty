---
# Metadata of the HTML header.
language: en
author: Romain Wallon
description: An example of the Neaty HTML template for Pandoc.
keywords:
    - example
    - neaty
    - template
    - pandoc
date: April 5th, 2021

# Document header.
logo: img/tm-neaty-logo.png
site-name: Neaty
title: Neaty HTML Template for Pandoc
header-image:
    path: img/neaty-01.jpg
    alt: Header image

# Document footer.
year: 2021
copyright-holder: Pandoc Toolkit
---

## First section

Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Sed ultricies feugiat purus ac luctus.
Fusce euismod enim mi, at imperdiet quam lobortis sit amet.
Mauris vulputate libero a mauris ultricies pellentesque.
Sed non egestas lorem.
Pellentesque ut libero feugiat, elementum ex ut, maximus magna.
Aenean pharetra lobortis auctor.
Integer elit augue, commodo porta condimentum vitae, volutpat dignissim ex.
Duis vel libero porttitor, ultrices est ac, sodales augue.
Sed id enim vitae elit viverra congue nec a odio.
Nulla tincidunt, orci vitae ultrices hendrerit, arcu elit feugiat nibh,
ornare facilisis ante erat ut ligula.
Sed lectus magna, tempus tempor purus quis, tincidunt sagittis ligula.
Proin blandit est et feugiat aliquet.
Mauris tellus dolor, elementum eget molestie nec, eleifend ac sem.

```html
<!-- An example of code block. -->
<div class="foo bar">
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
</div>
```

Sed tempor scelerisque elit in vulputate.
Curabitur et orci a odio suscipit luctus.
Nullam dignissim convallis auctor.
Maecenas ut elit ultrices, tincidunt nisl a, imperdiet est.
Quisque tincidunt libero egestas, pulvinar.

![Lorem ipsum dolor sit amet](img/neaty-02.jpg)

## Second section

Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Sed ultricies feugiat purus ac luctus.
Fusce euismod enim mi, at imperdiet quam lobortis sit amet.
Mauris vulputate libero a mauris ultricies pellentesque.
Sed non egestas lorem.
Pellentesque ut libero feugiat, elementum ex ut, maximus magna.
Aenean pharetra lobortis auctor.
Integer elit augue, commodo porta condimentum vitae, volutpat dignissim ex.
Duis vel libero porttitor, ultrices est ac, sodales augue.
Sed id enim vitae elit viverra congue nec a odio.
Nulla tincidunt, orci vitae ultrices hendrerit, arcu elit feugiat nibh,
ornare facilisis ante erat ut ligula.
Sed lectus magna, tempus tempor purus quis, tincidunt sagittis ligula.
Proin blandit est et feugiat aliquet.
Mauris tellus dolor, elementum eget molestie nec, eleifend ac sem.

![Lorem ipsum dolor sit amet](img/neaty-03.jpg)

Sed tempor scelerisque elit in vulputate.
Curabitur et orci a odio suscipit luctus.
Nullam dignissim convallis auctor.
Maecenas ut elit ultrices, tincidunt nisl a, imperdiet est.
Quisque tincidunt libero egestas, pulvinar.

## Third section

Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Sed ultricies feugiat purus ac luctus.
Fusce euismod enim mi, at imperdiet quam lobortis sit amet.
Mauris vulputate libero a mauris ultricies pellentesque.
Sed non egestas lorem.
Pellentesque ut libero feugiat, elementum ex ut, maximus magna.
Aenean pharetra lobortis auctor.
Integer elit augue, commodo porta condimentum vitae, volutpat dignissim ex.
Duis vel libero porttitor, ultrices est ac, sodales augue.
Sed id enim vitae elit viverra congue nec a odio.
Nulla tincidunt, orci vitae ultrices hendrerit, arcu elit feugiat nibh,
ornare facilisis ante erat ut ligula.
Sed lectus magna, tempus tempor purus quis, tincidunt sagittis ligula.
Proin blandit est et feugiat aliquet.
Mauris tellus dolor, elementum eget molestie nec, eleifend ac sem.

![Lorem ipsum dolor sit amet](img/neaty-04.jpg)

Sed tempor scelerisque elit in vulputate.
Curabitur et orci a odio suscipit luctus.
Nullam dignissim convallis auctor.
Maecenas ut elit ultrices, tincidunt nisl a, imperdiet est.
Quisque tincidunt libero egestas, pulvinar.

![Lorem ipsum dolor sit amet](img/neaty-05.jpg)

## Fourth section

Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Sed ultricies feugiat purus ac luctus.
Fusce euismod enim mi, at imperdiet quam lobortis sit amet.
Mauris vulputate libero a mauris ultricies pellentesque.
Sed non egestas lorem.
Pellentesque ut libero feugiat, elementum ex ut, maximus magna.
Aenean pharetra lobortis auctor.
Integer elit augue, commodo porta condimentum vitae, volutpat dignissim ex.
Duis vel libero porttitor, ultrices est ac, sodales augue.
Sed id enim vitae elit viverra congue nec a odio.
Nulla tincidunt, orci vitae ultrices hendrerit, arcu elit feugiat nibh,
ornare facilisis ante erat ut ligula.
Sed lectus magna, tempus tempor purus quis, tincidunt sagittis ligula.
Proin blandit est et feugiat aliquet.
Mauris tellus dolor, elementum eget molestie nec, eleifend ac sem.

![Lorem ipsum dolor sit amet](img/neaty-06.jpg)

Sed tempor scelerisque elit in vulputate.
Curabitur et orci a odio suscipit luctus.
Nullam dignissim convallis auctor.
Maecenas ut elit ultrices, tincidunt nisl a, imperdiet est.
Quisque tincidunt libero egestas, pulvinar.
